<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Workload */
?>
<div class="workload-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'box_id',
            'status',
            'min_count',
            'start_period',
            'end_period',
        ],
    ]) ?>

</div>
