<?php

use app\models\Box;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
//         [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'id',
//     ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "Фото",
        'content' => function ($data) {
            $imgLink =  "https://netdolgam.info/images1/kupitkvartiruvnovostroykaxpermiotzastroy_FD9BC081.jpg";
            return Html::a(Html::img(
                $data->photo ? '/'.$data->photo : $imgLink  ,
                ['alt' => 'message user image', 'class' => 'direct-chat-img','style' => 'width: 100px;']),$data->photo ? '/'.$data->photo : $imgLink,['target'=>'_blank', 'data-pjax'=>"0"]);


        }
    ],


    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'box_id',
        'content' => function($data){
            $box = Box::find()->where(['id' => $data->box_id])->one();
            return ' ' . Html::a($box->name,
                    $box->path, ['target'=>'_blank', 'data-pjax'=>"0"]);

        },
        'filter' => ArrayHelper::map(Box::find()->where(['company_id' => Yii::$app->user->identity->company_id])->asArray()->all(), 'id', 'name'),
        'filterType' => 'dropdown',
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
    ],
    [
        'attribute'=>'status',
        'label' => 'Статус',
        'filterType' => 'dropdown',
        'filter'=>array(
            1 => 'Свободен',
            0 => 'Занят',
            2 => 'Камера не отвечает',
        ),
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
        'content' => function($model){
            $icon = '';
            if($model->status == 0){
                $icon = 'Занят';
            } else if($model->status == 1) {
                $icon = 'Свободен';
            } else if($model->status == 2) {
                $icon = 'Камера не отвечает';
            }

            return $icon;
        },
        'width' => '1%',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'min_count',
    ],
    [   'attribute' => 'start_period',
        'filterType'=> \kartik\grid\GridView::FILTER_DATE,

        'filterWidgetOptions' => [

            'options' => ['placeholder' => 'Выберите дату'],

            'pluginOptions' => [

                'format' => 'yyyy-mm-dd',

                'todayHighlight' => true

            ]

        ],

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_period',
        'filterType'=> \kartik\grid\GridView::FILTER_DATE,

        'filterWidgetOptions' => [

            'options' => ['placeholder' => 'Выберите дату'],

            'pluginOptions' => [

                'timePicker' => false,

                'format' => 'yyyy-mm-dd',

                'todayHighlight' => true

            ]

        ],

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'info',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Удаление',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   