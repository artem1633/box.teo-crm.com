<?php

use app\models\Box;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Workload */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workload-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if(Yii::$app->user->identity->isSuperAdmin() == false){?>
    <?= $form->field($model, 'box_id')->dropDownList(ArrayHelper::map(Box::find()->where(['company_id' => Yii::$app->user->identity->company_id])->orderBy('name asc')->all(), 'id', 'name'))?>
    <?php }else{?>
    <?= $form->field($model, 'box_id')->dropDownList(ArrayHelper::map(Box::find()->orderBy('name asc')->all(), 'id', 'name'))?>
    <?php }?>
    <?= $form->field($model, 'status')->dropDownList([
            0 => 'Занят',
            1 => 'Свободен'
    ]) ?>

    <?= $form->field($model, 'min_count')->textInput() ?>

    <?= $form->field($model,'start_period')->widget(
        DateTimePicker::className(),[
            'options' => ['placeholder' => 'Выберите дату и время ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'todayHighlight' => true
            ]
        ]
    )?>
    <?= $form->field($model,'end_period')->widget(
        DateTimePicker::className(),[
            'options' => ['placeholder' => 'Выберите дату и время ...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'todayHighlight' => true
            ]
        ]
    )?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
