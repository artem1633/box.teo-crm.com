<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Workload */

?>
<div class="workload-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
