<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Box */
?>
<div class="box-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'token',
            'path',
        ],
    ]) ?>

</div>
