<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Box */
?>
<div class="box-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
