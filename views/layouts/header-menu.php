<?php

use app\admintheme\widgets\Menu;

?>

<div id="sidebar" class="sidebar">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo Menu::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav'],
                    'items' => [
                        //                    ['label' => 'Пользователи', 'icon' => 'fa fa-user-o', 'url' => ['/user'],],
                        // ['label' => 'Кабинет администратора', 'url' => ['/admin-files'], 'icon' => 'fa fa-file', 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                        [
                            'label' => 'Моя компания',
                            'url' => ['/company'],
                            'icon' => 'fa fa-building-o',
                            'visible' => Yii::$app->user->identity->isSuperAdmin() == false
                        ],
                        [
                            'label' => 'Бокс',
                            'url' => ['/box'],
                            'icon' => 'fa fa-archive',
                        ],
                        [
                            'label' => 'Загруженость',
                            'url' => ['/workload'],
                            'icon' => 'fa fa-battery-three-quarters',
                        ],
                        [
                            'label' => 'Отчет',
                            'url' => ['/report'],
                            'icon' => 'fa fa-area-chart',
                        ],
                        ['label' => 'Пользователи', 'url' => ['/user'], 'icon' => 'fa fa-users'],
                        [
                            'label' => 'Компании',
                            'icon' => 'fa fa-building-o',
                            'url' => ['/company'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
//                        ['label' => 'Обратная связь', 'url' => ['/ticket'], 'icon' => 'fa fa-question'],
//                        [
//                            'label' => 'Справочники',
//                            'icon' => 'fa fa-book',
//                            'url' => '#',
//                            'options' => ['class' => 'has-sub'],
//                            'items' => [
//                                ['label' => 'Должности', 'url' => ['/position']],
//                                [
//                                    'label' => 'Подписки',
//                                    'url' => ['/subscription'],
//                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
//                                ],
//                                [
//                                    'label' => 'Валюта',
//                                    'url' => ['/currency'],
//                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
//                                ],
//                                [
//                                    'label' => 'Кред. продукты',
//                                    'url' => ['/product-type'],
//                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
//                                ],
//                                [
//                                    'label' => 'Статусы должника',
//                                    'url' => ['/debtor-status'],
//                                    'visible' => Yii::$app->user->identity->isSuperAdmin()
//                                ],
//                                [
//                                    'label' => 'Документация АПИ',
//                                    'url' => ['/api/doc'],
//                                    'visible' => Yii::$app->user->identity->company->api_key ?? false
//                                ],
//                            ],
//                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
