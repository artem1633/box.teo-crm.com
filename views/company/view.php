<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Company */


CrudAsset::register($this);

?>
<div class="company-view">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Основная информация</h4>
                </div>
                <div class="panel-body">
                    <?php Pjax::begin(['id' => 'company-view-pjax']) ?>
                    <div class="col-md-12">
                        <h4>Реквизиты</h4>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'name',
                                'email',
                            ],
                        ]) ?>
                    </div>
                    <?php Pjax::end() ?>

                </div>
                <div class="panel-footer">
                    <?= Html::a('Редактировать',
                        ['update', 'id' => $model->id, 'pjaxContainer' => '#company-view-pjax'],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote']) ?>
                    <?php if (Yii::$app->user->identity->isSuperAdmin() && $model->moderated == 0): ?>
                        <?= Html::a('Одобрить к торгам',
                            ['moderate', 'id' => $model->id, 'pjaxContainer' => '#company-view-pjax'], [
                                'class' => 'btn btn-success',
                                'role' => 'modal-remote',
                                'title' => 'Одобрить к торгам компанию',
                                'data-confirm' => false,
                                'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Вы уверены?',
                                'data-confirm-message' => "Вы действительно хотите одобрить компанию «{$model->name}» к торгам?"
                            ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>

</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
