<?php

use app\models\Box;
use app\models\Workload;
use app\models\WorkloadSearch;
use kartik\select2\Select2;
use skeeks\widget\highcharts\Highcharts;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/**
 * @var $report array
 * @var $model \app\models\Box
 */
//echo "<pre>";
//print_r();
//echo "</pre>"
?>


<div class="panel panel-success">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Фильтры</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(['method' => 'GET']); ?>

                    <div class="col-md-4">
                        <?= $form->field($searchModel, 'box_id')->widget(Select2::class, [
                            'data' =>Yii::$app->user->identity->isSuperAdmin() ? ArrayHelper::map(Box::find()->all(), 'id', 'name') : ArrayHelper::map(Box::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(),'id','name'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'placeholder' => 'Выберите',
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($searchModel, 'start_period')->widget(\kartik\grid\GridView::FILTER_DATE,[
                                'options' => ['placeholder' => 'Выберите дату'],
                                'pluginOptions' => [

                                    'timePicker' => false,

                                    'format' => 'yyyy-mm-dd',

                                    'todayHighlight' => true

                                ]
                            ]
                        ) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($searchModel, 'end_period')->widget(\kartik\grid\GridView::FILTER_DATE,[
                                'options' => ['placeholder' => 'Выберите дату'],
                                'pluginOptions' => [

                                    'timePicker' => false,

                                    'format' => 'yyyy-mm-dd',

                                    'todayHighlight' => true

                                ]
                            ]
                        ) ?>
                    </div>

                    <div class="col-md-12">
                        <?= Html::submitButton('Применить', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end() ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" >
                <div class="panel-heading" style="background-color: #242a30;color: white">
                    <h4 class="panel-title">Статистика</h4>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" style="padding:0px;">
                        <thead>
                        <tr>
                            <th>Бокс</th>
                            <th>Свободно</th>
                            <th>Занят</th>
                            <th>Кол-во машин</th>
                            <th>Средняя на машину</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $get = Yii::$app->request->get('WorkloadSearch');
                        $boxId = $get['box_id'];

                        if(Yii::$app->user->identity->isSuperAdmin() == false){
                            if($boxId != null) {
                                $contactAll = Box::find()->where(['company_id' => Yii::$app->user->identity->company_id])->andFilterWhere(['id' => $boxId])->all();
                            }else{
                                $contactAll = Box::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
                            }

                        }else{
                            if ($boxId != null){
                                $contactAll = Box::find()->where(['id' => $boxId])->all();
                            }else{
                                $contactAll = Box::find()->all();
                            }
                        }
                        $counter = 0;
                        $all = 0;
                        foreach ($contactAll as $item) {

                            if (isset($get['end_period']) or isset($get['start_period']) ){
                                if (!isset($get['end_period']) ){
                                    $sumBlockCountBlock = Workload::find()->where(['box_id' => $item->id, 'status' => 0])->andFilterWhere(['>=', 'start_period', $get['start_period']])->count(); // занят
                                    $sumBlock = round(Workload::find()->where(['box_id' => $item->id, 'status' => 0])->andFilterWhere(['>=', 'start_period', $get['start_period']])->sum('min_count')); // занят
                                    $sumBlockM = round($sumBlock / 60);
                                    if($sumBlockCountBlock>0){
                                        if($sumBlock != null and $sumBlockCountBlock !=null){
                                            $sumBlockH = round($sumBlock / $sumBlockCountBlock);
                                        }else{
                                            $sumBlockH = 0;
                                        }
                                    }
                                    $sumFree = round(Workload::find()->where(['box_id' => $item->id, 'status' => 1])->andFilterWhere(['>=', 'start_period', $get['start_period']])->sum('min_count')); // свободен
                                    $sumFreeM = round($sumFree / 60);
                                }elseif (!isset($get['start_period'])){
                                    $sumBlockCountBlock = Workload::find()->where(['box_id' => $item->id, 'status' => 0])->andFilterWhere(['<=', 'end_period', $get['end_period']])->count(); // занят
                                    $sumBlock = round(Workload::find()->where(['box_id' => $item->id, 'status' => 0])->andFilterWhere(['<=', 'end_period', $get['end_period']])->sum('min_count')); // занят
                                    $sumBlockM = round($sumBlock / 60);
                                    if($sumBlockCountBlock>0){
                                        if($sumBlock != null and $sumBlockCountBlock !=null){
                                            $sumBlockH = round($sumBlock / $sumBlockCountBlock);
                                        }else{
                                            $sumBlockH = 0;
                                        }
                                    }
                                    $sumFree = round(Workload::find()->where(['box_id' => $item->id, 'status' => 1])->andFilterWhere(['<=', 'end_period', $get['end_period']])->sum('min_count')); // свободен
                                    $sumFreeM = round($sumFree / 60);
                                }elseif (isset($get['end_period']) and isset($get['start_period'])){
                                    $sumBlockCountBlock = Workload::find()->where(['box_id' => $item->id, 'status' => 0])->andFilterWhere(['>=', 'start_period', $get['start_period']])->andFilterWhere(['<=', 'end_period', $get['end_period']])->count(); // занят
                                    $sumBlock = round(Workload::find()->where(['box_id' => $item->id, 'status' => 0])->andFilterWhere(['>=', 'start_period', $get['start_period']])->andFilterWhere(['<=', 'end_period', $get['end_period']])->sum('min_count')); // занят
                                    $sumBlockM = round($sumBlock / 60);
                                    if($sumBlockCountBlock>0){
                                        if($sumBlock != null and $sumBlockCountBlock !=null){
                                            $sumBlockH = round($sumBlock / $sumBlockCountBlock);
                                        }else{
                                            $sumBlockH = 0;
                                        }
                                    }
                                    $sumFree = round(Workload::find()->where(['box_id' => $item->id, 'status' => 1])->andFilterWhere(['>=', 'start_period', $get['start_period']])->andFilterWhere(['<=', 'end_period', $get['end_period']])->sum('min_count')); // свободен
                                    $sumFreeM = round($sumFree / 60);
                                }

                            }
                            elseif ($get['start_period'] == null or $get['end_period'] == null){
                                $sumBlockCountBlock = Workload::find()->where(['box_id' => $item->id, 'status' => 0])->count(); // занят
                                $sumBlock = round(Workload::find()->where(['box_id' => $item->id, 'status' => 0])->sum('min_count')); // занят
                                $sumBlockM = round($sumBlock / 60);
                                if($sumBlockCountBlock>0){
                                    if($sumBlock != null and $sumBlockCountBlock !=null){
                                        $sumBlockH = round($sumBlock / $sumBlockCountBlock);
                                    }else{
                                        $sumBlockH = 0;
                                    }
                                }
                                $sumFree = round(Workload::find()->where(['box_id' => $item->id, 'status' => 1])->sum('min_count')); // свободен
                                $sumFreeM = round($sumFree / 60);
                            }
//                            $sumBlockCountBlock = Workload::find()->where(['box_id' => $item->id, 'status' => 0])->count(); // занят
//                            $sumBlock = round(Workload::find()->where(['box_id' => $item->id, 'status' => 0])->sum('min_count')); // занят
//                            $sumBlockM = round($sumBlock / 60);
//                            if($sumBlockCountBlock>0){
//                                $sumBlockH = round($sumBlock / $sumBlockCountBlock);
//                            }
//
//
//                            $sumFree = round(Workload::find()->where(['box_id' => $item->id, 'status' => 1])->sum('min_count')); // свободен
//                            $sumFreeM = round($sumFree / 60);



                            echo "
                                <tr>
                                    <td>{$item->name}</td>
                                    <td>Минут: {$sumFree}, Часов: {$sumFreeM}</td>
                                    <td>Минут: {$sumBlock}, Часов: {$sumBlockM} </td>
                                    <td>{$sumBlockCountBlock}</td>
                                    <td>{$sumBlockH}</td>
        
                                </tr>";
                            $counter++;
                            }
                        ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
