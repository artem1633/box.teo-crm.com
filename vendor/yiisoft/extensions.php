<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.9.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable',
    ),
  ),
  'kartik-v/yii2-widget-colorinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-colorinput',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/color' => $vendorDir . '/kartik-v/yii2-widget-colorinput',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.4.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead/src',
    ),
  ),
  'perminder-klair/yii2-dropzone' => 
  array (
    'name' => 'perminder-klair/yii2-dropzone',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kato' => $vendorDir . '/perminder-klair/yii2-dropzone',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.7.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '1.3.4.0',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x',
    ),
  ),
  'kartik-v/yii2-editable' => 
  array (
    'name' => 'kartik-v/yii2-editable',
    'version' => '1.7.6.0',
    'alias' => 
    array (
      '@kartik/editable' => $vendorDir . '/kartik-v/yii2-editable',
    ),
  ),
  'johnitvn/yii2-ajaxcrud' => 
  array (
    'name' => 'johnitvn/yii2-ajaxcrud',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@johnitvn/ajaxcrud' => $vendorDir . '/johnitvn/yii2-ajaxcrud/src',
    ),
    'bootstrap' => 'johnitvn\\ajaxcrud\\Bootstrap',
  ),
  'johnitvn/yii2-settings' => 
  array (
    'name' => 'johnitvn/yii2-settings',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@johnitvn/settings' => $vendorDir . '/johnitvn/yii2-settings/src',
    ),
  ),
  'kartik-v/yii2-date-range' => 
  array (
    'name' => 'kartik-v/yii2-date-range',
    'version' => '1.6.8.0',
    'alias' => 
    array (
      '@kartik/daterange' => $vendorDir . '/kartik-v/yii2-date-range',
    ),
  ),
  'main-kuler/yii2-ajaxcrud' => 
  array (
    'name' => 'main-kuler/yii2-ajaxcrud',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@johnitvn/ajaxcrud' => $vendorDir . '/main-kuler/yii2-ajaxcrud/src',
    ),
    'bootstrap' => 'johnitvn\\ajaxcrud\\Bootstrap',
  ),
  'mihaildev/yii2-ckeditor' => 
  array (
    'name' => 'mihaildev/yii2-ckeditor',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@mihaildev/ckeditor' => $vendorDir . '/mihaildev/yii2-ckeditor',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'unclead/yii2-multiple-input' => 
  array (
    'name' => 'unclead/yii2-multiple-input',
    'version' => '2.22.0.0',
    'alias' => 
    array (
      '@unclead/multipleinput/examples' => $vendorDir . '/unclead/yii2-multiple-input/examples',
      '@unclead/multipleinput' => $vendorDir . '/unclead/yii2-multiple-input/src',
      '@unclead/multipleinput/tests' => $vendorDir . '/unclead/yii2-multiple-input/tests',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '1.4.4.0',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'rmrevin/yii2-comments' => 
  array (
    'name' => 'rmrevin/yii2-comments',
    'version' => '1.4.4.0',
    'alias' => 
    array (
      '@rmrevin/yii/module/Comments' => $vendorDir . '/rmrevin/yii2-comments',
    ),
  ),
);
