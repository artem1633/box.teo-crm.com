<?php

namespace app\controllers;

use app\models\AdminFiles;
use Yii;
use yii\base\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Class AdminFilesController
 * @package app\controllers
 */
class AdminFilesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = AdminFiles::find()->one();

        if($model == null){
            $model = new AdminFiles();
        }

        if($model->load($request->post()) && $model->validate()){

            if(file_exists($model->contract_creator)){
                unlink($model->contract_creator);
            }

            $file = UploadedFile::getInstance($model,'contract_creator');

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();
            $path = "uploads/$name.$file->extension";
            $file->saveAs($path);
            $model->contract_creator = $path;


            if(file_exists($model->contract_winner)){
                unlink($model->contract_winner);
            }

            $file = UploadedFile::getInstance($model,'contract_winner');

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();
            $path = "uploads/$name.$file->extension";
            $file->saveAs($path);
            $model->contract_winner = $path;

            if($model->save()){
                Yii::$app->session->setFlash('success', 'Изменения успешно сохранены');
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}