<?php

namespace app\controllers;

use app\filters\PermissionsFilter;
use app\models\Box;
use app\models\BoxSearch;
use app\models\DailyReport;
use app\models\RegistrationReportFilter;
use app\models\UsersList;
use app\models\WorkloadSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * GlobalMessagesController implements the CRUD actions for GlobalMessages model.
 */
class ReportController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GlobalMessages models.
     * @return mixed
     */
    public function actionIndex()
    {
            $searchModel = new WorkloadSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $dataProvider->pagination = false;


            return $this->render('index',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);


    }
}
