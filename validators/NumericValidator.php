<?php

namespace app\validators;

use Yii;
use yii\validators\Validator;
use app\models\Clients;

/**
 * Class UniquePhoneValidator
 * @package app\validators
 */
class NumericValidator extends Validator
{
    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $model->$attribute = str_replace(' ', '', $model->$attribute);
        $model->$attribute = str_replace(',', '.', $model->$attribute);
    }
}

?>