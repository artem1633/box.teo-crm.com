<?php

namespace app\modules\api\controllers;

use app\models\Box;
use app\models\Workload;
use app\models\DebtorStatus;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Debtor controller for the `api` module
 */
class V1Controller extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Добавляет должника и дело
     * @return array
     */
    public function actionList()
    {
        $list = Box::find()->all();
        return $list;
    }


    /**
     * Добавляет должника и дело
     * @return array
     */
    public function actionAdd($token, $status, $photo = null, $info = null)
    {
        if ($status == 2){
            return ['success' => 1, 'error' => 'Не отвечает камера'];
        }

        $box =  Box::find()->where(['token' => $token])->one();

        if (!$box) {
            return ['success' => 0, 'error' => 'Токен доступа не опознан'];
        }
        $start = $box->time_start;
        $end = $box->time_end;
        if (date('H:i:s') < $start and date('H:i:s') > $end) {
            $workload = Workload::find()->where(['box_id' => $box->id])->orderBy(['id' => SORT_DESC])->one();
            if ($workload->end_period == null) {
                $workload->end_period = date('Y-m-d H:i:s');
                $workload->save();
            }
            return ['success' => 0, 'error' => 'Бокс не работает'];
        }
        $workload = Workload::find()->where(['box_id' => $box->id])->orderBy(['id' => SORT_DESC])->one();
        if (!$workload or $workload->end_period != null) {
            if ($photo != null) {
                $path = 'img/'.Yii::$app->security->generateRandomString(12).'logo.jpg';
                file_put_contents($path, file_get_contents($photo));
                $photo = $path;
            }

            (new Workload([
                'box_id' => $box->id,
                'status' => $status,
                'photo' => $photo,
                'info' => $info,
                'start_period' => date('Y-m-d H:i:s'),
            ]))->save();
            return ['success' => 1, 'error' => 'Добавили запись'];
        } else {
            if ($workload->status != $status) {

                if ($photo != null) {
                    $path = 'img/'.Yii::$app->security->generateRandomString(12).'logo.jpg';
                    file_put_contents($path, file_get_contents($photo));
                    $photo = $path;
                }


                $workload->end_period = date('Y-m-d H:i:s');
                $date1 = strtotime($workload->start_period);
                $date2 = strtotime($workload->end_period);
                $workload->min_count = round(ABS(($date1 - $date2)/60));
//                var_dump($workload->min_count);
//                exit();
                $workload->save();
                (new Workload([
                    'box_id' => $box->id,
                    'status' => $status,
                    'photo' => $photo,
                    'info' => $info,
                    'start_period' => date('Y-m-d H:i:s'),
                ]))->save();
                return ['success' => 1, 'error' => 'Статус изменился'];
            } else {

                if ($info != 'number isn`t found' && $workload->info == 'number isn`t found') {
                    $workload->info = $info;
                    $workload->save();
                }
                return ['success' => 1, 'error' => 'Статус не изменился'];
            }
        }

        return ['success' => 0, 'error' => 'Что то пошло не так'];

    }

}
