<?php

namespace app\base;

use app\queries\CompanyQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

/**
 * Class CompanyActiveRecord
 * @package app\base
 */
class CompanyActiveRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if($this->hasAttribute('company_id') && Yii::$app->user->getIsGuest() == false){
            return [
                'company' => [
                    'class' => BlameableBehavior::className(),
                    'createdByAttribute' => 'company_id',
                    'updatedByAttribute' => null,
                    'value' => function($event){
                        if(Yii::$app->user->identity != null){
                            if(Yii::$app->user->identity->company_id != null){
                                return Yii::$app->user->identity->company_id;
                            }
                        }
                    }
                ],
            ];
        } else {
            return parent::behaviors();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class(), Yii::$app->user->identity);
    }
}