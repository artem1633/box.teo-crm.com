<?php

use yii\db\Migration;

/**
 * Class m200910_131648_add_box_info
 */
class m200910_131648_add_box_info extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('box', 'info', $this->string()->comment('Инфо'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('box', 'info');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200910_131648_add_box_info cannot be reverted.\n";

        return false;
    }
    */
}
