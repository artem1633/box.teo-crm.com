<?php

use yii\db\Migration;

/**
 * Class m200905_142839_add_workload_column
 */
class m200905_142839_add_workload_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('workload', 'photo', $this->string()->comment('фото'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('workload', 'photo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200905_142839_add_workload_column cannot be reverted.\n";

        return false;
    }
    */
}
