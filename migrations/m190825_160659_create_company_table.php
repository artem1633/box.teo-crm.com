<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m190825_160659_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'email' => $this->string()->comment('Электронный адрес'),
            'is_super_company' => $this->boolean()->defaultValue(false)->comment('Является ли супер компанией'),
            'created_at' => $this->dateTime(),
        ]);



        $this->insert('company', [
            'name' => 'Супер компания',
            'is_super_company' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('company');
    }
}
