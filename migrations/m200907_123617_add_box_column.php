<?php

use yii\db\Migration;

/**
 * Class m200907_123617_add_box_column
 */
class m200907_123617_add_box_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('box', 'ip', $this->string()->comment('АйПи'));
        $this->addColumn('box', 'login', $this->string()->comment('Логин'));
        $this->addColumn('box', 'pass', $this->string()->comment('Пароль'));
        $this->addColumn('box', 'time_start', $this->time()->comment('Время начала'));
        $this->addColumn('box', 'time_end', $this->time()->comment('Время окончания'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('box', 'ip');
        $this->dropColumn('box', 'login');
        $this->dropColumn('box', 'pass');
        $this->dropColumn('box', 'time_start');
        $this->dropColumn('box', 'time_end');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_123617_add_box_column cannot be reverted.\n";

        return false;
    }
    */
}
