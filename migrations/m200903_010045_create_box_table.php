<?php

use yii\db\Migration;

/**
 * Handles the creation of table `box`.
 */
class m200903_010045_create_box_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('box', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'token' => $this->string()->comment('Токен'),
            'path' => $this->string()->comment('Путь к папке'),
            'company_id' => $this->integer()->comment('Компания')
        ]);
        $this->createIndex(
            'idx-box-company_id',
            'box',
            'company_id'
        );
        $this->addForeignKey(
            'fk-box-company_id',
            'box',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-box-company_id',
            'box'
        );

        $this->dropIndex(
            'idx-box-company_id',
            'box'
        );
        $this->dropTable('box');
    }
}
