<?php

use yii\db\Migration;

/**
 * Handles the creation of table `workload`.
 */
class m200903_010346_create_workload_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('workload', [
            'id' => $this->primaryKey(),
            'box_id' => $this->integer()->comment('id бокса'),
            'status' => $this->boolean()->comment('Статус'),
            'min_count' => $this->integer()->comment('Количество минут'),
            'start_period' => $this->dateTime()->comment('Дата и время начала'),
            'end_period' => $this->dateTime()->comment('Дата и время окончания')
        ]);
        $this->createIndex(
            'idx-workload-box_id',
            'workload',
            'box_id'
        );
        $this->addForeignKey(
            'fk-workload-box_id',
            'workload',
            'box_id',
            'box',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-workload-box_id',
            'workload'
        );

        $this->dropIndex(
            'idx-workload-box_id',
            'workload'
        );
        $this->dropTable('workload');
    }
}
