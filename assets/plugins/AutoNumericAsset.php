<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class AutoNumericAsset
 * @package app\assets
 */
class AutoNumericAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'plugins/numeric/autoNumeric.js'
    ];
    public $depends = [
        'app\assets\AppAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
