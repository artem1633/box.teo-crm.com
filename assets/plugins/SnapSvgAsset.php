<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class D3Asset
 * @package app\plugins\assets
 */
class SnapSvgAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'libs/snap/snap.svg-min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
