<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Workload]].
 *
 * @see Workload
 */
class WorkloadQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Workload[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Workload|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
