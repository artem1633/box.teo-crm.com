<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%workload}}".
 *
 * @property int $id
 * @property int $box_id id бокса
 * @property int $status Статус
 * @property int $min_count Количество минут
 * @property string $start_period Дата и время начала
 * @property string $end_period Дата и время окончания
 * @property string $info инфо
 *
 *
 *
 * @property Box $box
 */
class Workload extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%workload}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['box_id', 'status', 'min_count'], 'integer'],
            [['start_period', 'end_period', 'photo','info'], 'safe'],
            [['box_id'], 'exist', 'skipOnError' => true, 'targetClass' => Box::className(), 'targetAttribute' => ['box_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'box_id' => 'Бокс',
            'status' => 'Статус',
            'min_count' => 'Количество минут',
            'start_period' => 'Дата и время начала',
            'end_period' => 'Дата и время окончания',
            'photo' => 'Фото',
            'info' => 'Инфо'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /**
     * @inheritdoc
     * @return WorkloadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WorkloadQuery(get_called_class());
    }
}
