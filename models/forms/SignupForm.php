<?php
namespace app\models\forms;

use app\models\Company;
use yii\base\Model;
use app\models\User;
use yii\behaviors\BlameableBehavior;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $userId;
    public $email;
    public $companyName;
    public $name;
    public $password;
    public $last_name;
    public $oferta;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['companyName', 'last_name', 'name', 'email', 'password'], 'required'],
            ['oferta', function(){
                if($this->oferta == 0){
                    $this->addError('oferta', 'Вы должны согласиться с договором публичной оферты');
                    return false;
                }
            }],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'email', 'message' => 'Этот email уже зарегистрирован',],
//            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже зарегистрирован',
//                'when' => function($model, $attribute){
//                    if($this->userId != null)
//                    {
//                        $userModel = User::findOne($this->userId);
//                        return $this->{$attribute} !== $userModel->getOldAttribute($attribute);
//                    }
//                    return true;
//                },
//            ],
            ['password', 'string', 'min' => 6],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['update'] = ['password', 'email'];//Scenario Values Only Accepted
    //     return $scenarios;
    // }

    public function attributeLabels()
    {
        return [
            'companyName' => 'Наименование компании',
            'password' => 'Пароль',
            'email' => 'Email',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
        ];
    }

    /**
     * Signs user up.
     *
     * @return true
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $company = new Company([
            'name' => $this->companyName,
            'email' => $this->email,
        ]);
        $companySave = $company->save(false);


        $user = new User();
        $user->detachBehavior('company');
        $user->name = $this->name;
        $user->last_name = $this->last_name;
        $user->setPassword($this->password);
        $user->company_id = $company->id;
        $user->email = $this->email;
        $user->is_company_super_admin = 1;

        $userSave = $user->save(false);

        return ($companySave && $userSave);
    }

    /**
     * update user.
     *
     * @param User $user
     * @return User|null the saved model or null if saving fails
     */
    public function update($user)
    {
        if (!$this->validate()) {
            return null;
        }

        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;
        // $user->generateAuthKey();
        
        return $user->update();
    }
}
