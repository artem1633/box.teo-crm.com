<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $is_super_company Является ли супер компанией
 * @property string $created_at
 * @property string $email Email

 */
class Company extends ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_super_company'], 'integer'],
            [['created_at',], 'safe'],
            [['name','email',],'string','max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'is_super_company' => 'Является ли супер компанией',
            'created_at' => 'Дата и время создания',
            'email' => 'Email',
        ];
    }



    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
//        if ($this->isNewRecord) {
//            $this->generateCodeRecursive();
//        }

        return parent::beforeSave($insert);
    }




    /**
     * @param integer $i
     * @throws \yii\base\Exception
     */
//    private function generateCodeRecursive($i = 1)
//    {
//        if ($i > 20) {
//            return;
//        }
//
//        $code = Yii::$app->security->generateRandomString(5);
//        $company = self::find()->where(['code' => $code])->one();
//        if ($company == null) {
//            $this->code = $code;
//            return;
//        } else {
//            $i++;
//            $this->generateCodeRecursive($i);
//        }
//    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }
	
	
	
	
}
